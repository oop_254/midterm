package com.waraporn.midterm;


public class MyBook {
    private String nameBook;
    private String page;
    private String chapter;

    public MyBook() {
        this.nameBook = "Tomie";
        this.page = "3";
        this.chapter = "Tomie's";
    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }
    public void setPage(String page) {
        this.page = page;
    }
    public void setChapter(String chapter) {
        this.chapter = chapter;
    }
    public void printTomie() {
        System.out.println("Name : "+this.nameBook);
        System.out.println("Page : "+this.page);
        System.out.println("Chapter : "+this.chapter);
    }
}