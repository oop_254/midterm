package com.waraporn.midterm;

public class MyFamily {
    private String dad;
    private String mom;
    private String bro;

    public MyFamily() {
        this.dad = "Kovit";
        this.mom = "Wannaporn";
        this.bro = "Paragon";
    }

    public void setDad(String dad) {
        this.dad = dad;
    }
    public void setMom(String mom) {
        this.mom = mom;
    }
    public void setBro(String bro) {
        this.bro = bro;
    }
    public void printFam() {
        System.out.println("My dad's name : "+this.dad);
        System.out.println("My mom's name : "+this.mom);
        System.out.println("My bro's name : "+this.bro);
    }
}
