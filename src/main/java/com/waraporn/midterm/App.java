package com.waraporn.midterm;

public class App {
    public static void main( String[] args ) {
        System.out.println("Class: 1");
        MyBook tomie = new MyBook();
        tomie.printTomie();
        
        System.out.println("");
        System.out.println("Class: 2");
        MyFamily warapornFam = new MyFamily();
        warapornFam.printFam();

    }
}
